import UIKit
func colorWith(red: Int, green: Int, blue: Int, alpha: CGFloat = 1.0) -> UIColor {
    return UIColor(red: CGFloat(red)/255.0, green: CGFloat(green)/0xff, blue: CGFloat(blue)/255.0, alpha: alpha)
}

func colorWith(hex: UInt, alpha: CGFloat = 1.0) -> UIColor {
    let red: CGFloat = CGFloat((hex >> 16) & 0xFF) / 0xFF
    let green: CGFloat = CGFloat((hex >> 8) & 0xFF) / 0xFF
    let blue: CGFloat = CGFloat(hex & 0xFF) / 0xFF
    return UIColor.init(red: red, green: green, blue: blue, alpha: alpha)

}

func colorWith(webHex: String) -> UIColor {
    let errorColor = UIColor(red: 0.72, green: 0.85, blue: 0.33, alpha: 1.0)
    guard webHex.hasPrefix("#") else {
        return errorColor
    }
    var alpha: CGFloat = 1.0

    //remove the hash mark
    var justTheNumbers = webHex.dropFirst()
    
    if justTheNumbers.count == 8 { //it's more than 6 so assume there is an alpha at the end
        alpha = CGFloat(strtoul("\(justTheNumbers.suffix(2))", nil, 16)) / 255.0
        justTheNumbers = justTheNumbers.dropLast(2)
    }
    
    guard justTheNumbers.count == 6 else {
        return errorColor
    }

    let firstTest = strtoul("\(justTheNumbers)", nil, 16)
    return colorWith(hex: firstTest, alpha: alpha)
}

let convertedColor = colorWith(webHex: "#ff8080")
let anotherColor = colorWith(hex: 0xffccaa)
let evenAnother = colorWith(red: 192, green: 192, blue: 192)
