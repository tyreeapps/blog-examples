import UIKit

var str = "Hello, playground"

class Base {}
class SubClassA : Base {}
class SubClassB : Base {}

func whatIs(it: Base) -> String {
    //option with switch
    switch it {
    case is SubClassB:
        return "Switch Subclass B"
    case is SubClassA:
        return "Switch Subclass A"
    default:
        assertionFailure("A new subclass has been introduced!")
    }
    //end option with switch
    
    //option with ifs
    //to get this part to execute, comment out the switch code above
    if it is SubClassA {
        return "IF SubClass A"
    }
    if it is SubClassB {
        return "IF SubClass B"
    }
    assertionFailure("A new subclass has been introduced!")
    return "Nothing"
    //end option with ifs
}

let test1 = SubClassB()
print(whatIs(it: test1))

